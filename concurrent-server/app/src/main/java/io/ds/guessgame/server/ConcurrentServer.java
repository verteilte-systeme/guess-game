package io.ds.guessgame.server;

import java.io.IOException;
import java.net.ServerSocket;

public class ConcurrentServer {
    public static final int SERVER_PORT = 7896;

    public static void main(String[] args) {
         
        try (ServerSocket gameSocket = new ServerSocket(SERVER_PORT)){            
            System.out.println("Guess Game Server up and running ...");
            while(true) {
                //Start a GuessGameThread when client connects                
                new GuessGameThread(gameSocket.accept()).start();                 
            }  
        } catch(IOException e) {
            System.err.println("Server: encountered connection problems. Cannot wait for connection requests!");
            e.printStackTrace();    
        } 
    }    
}
