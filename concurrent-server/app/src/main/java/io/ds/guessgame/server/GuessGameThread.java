package io.ds.guessgame.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Random;

public class GuessGameThread extends Thread {

    private final Socket gamerSocket;
    private static final Random RANDOM = new Random();
    private static int MAX_ATTEMPTS = 3;

    /*
     * Client Status Codes: 0 = Start Game
     * 
     * Server Status Codes: 0 = Server Ready, Game initialized 1 = Correct Guess,
     * Client wins! 2 = Incorrect Guess 9 = Game Over
     */
    private static final int START_GAME = 0;
    private static final int SERVER_READY = 0;
    private static final int CORRECT_GUESS = 1;
    private static final int INCORRECT_GUESS = 2;
    private static final int GAME_OVER = 9;

    public GuessGameThread(Socket gamerSocket) {
        this.gamerSocket = gamerSocket;
    }

    @Override
    public void run() {
        try (gamerSocket) {
            // Init communication streams and wait for message to start game
            InputStream fromClient = gamerSocket.getInputStream();
            OutputStream toClient = gamerSocket.getOutputStream();
            int clientCode = fromClient.read();

            if (clientCode == START_GAME) {
                System.out.print("Server-Thread starts a new game ...");
                // Pick random number, and send server ready (0)!
                int pick = RANDOM.nextInt(10);
                int attempts = 0;
                toClient.write(SERVER_READY);

                while (true) { // Start game loop
                    // Wait for first attempt
                    int guess = fromClient.read();
                    // Check guess
                    if (guess == pick) {
                        toClient.write(CORRECT_GUESS); // great!
                        break;
                    } else {
                        attempts++;
                        if (attempts < MAX_ATTEMPTS)
                            toClient.write(INCORRECT_GUESS); // wrong guess!
                        else {
                            toClient.write(GAME_OVER); // game over!
                            break;
                        }
                    }
                }
            } else {
                System.err.println("Received unexpected client code!");
            }
            gamerSocket.close();// close connection
            System.out.println("Game terminated!");
        } catch (IOException e) {
            System.err.println("Server: encountered connection problems. Cannot read or write to streams!");
        }
    }
}
