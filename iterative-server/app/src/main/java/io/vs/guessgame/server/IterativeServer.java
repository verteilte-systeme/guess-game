package io.vs.guessgame.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

public class IterativeServer {
    public static final int SERVER_PORT = 7896;
    private static final Random RANDOM = new Random();
    private static int MAX_ATTEMPTS = 3;

    /*
     * Just to improve readability of the code:
     *
     * Client Status Codes:
     * 0 = Start Game
     * 
     * Server Status Codes:
     * 0 = Server Ready, Game initialized
     * 1 = Correct Guess, Client wins!
     * 2 = Incorrect Guess
     * 9 = Game Over
     */
    private static final int START_GAME = 0;
    private static final int SERVER_READY = 0;
    private static final int CORRECT_GUESS = 1;
    private static final int INCORRECT_GUESS = 2;
    private static final int GAME_OVER = 9;

    public static void main(String[] args) {
        try (ServerSocket gameSocket = new ServerSocket(SERVER_PORT)) {
            System.out.println("Guess Game Server up and running ...");
            while (true) {
                // Wait for client access
                Socket gamerSocket = gameSocket.accept();
                try (gamerSocket) {
                    System.out.println("Server starts a new game ...");

                    // Init communication streams and wait for message to start a game
                    InputStream fromClient = gamerSocket.getInputStream();
                    OutputStream toClient = gamerSocket.getOutputStream();
                    int clientCode = fromClient.read();
                    if (clientCode == START_GAME) {                        
                        // Init game and send server ready (0)!
                        int pick = RANDOM.nextInt(10); // pick random number (0-9)                        
                        int attempts = 0; // init number of attempts
                        System.out.println("Server picked random number "+pick);
                        toClient.write(SERVER_READY);

                        while (true) {// enter game loop
                            // Wait for first attempt of client to guess number
                            int guess = fromClient.read();
                            // Check guess
                            if (guess == pick) {
                                toClient.write(CORRECT_GUESS); // great!
                                break;
                            } else {
                                attempts++;
                                if (attempts < MAX_ATTEMPTS)
                                    toClient.write(INCORRECT_GUESS); // wrong guess!
                                else {
                                    toClient.write(GAME_OVER); // game over!
                                    break;
                                }
                            }
                        }
                        System.out.println("Server: Game terminated!");
                        attempts = 0;
                    } else {
                        System.err.println("Server: Received unexpected client code! Closing connection!");
                        gamerSocket.close();                        
                    }                    
                } catch (IOException e) {
                    System.err.println("Server: encountered connection problems. Cannot wait for connection requests!");
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            System.err.println("Server: Can't create a ServerSocket!");            
            e.printStackTrace();
        }
    }
}
