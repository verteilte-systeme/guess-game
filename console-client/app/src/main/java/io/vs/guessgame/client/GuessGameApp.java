package io.vs.guessgame.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

public class GuessGameApp {
      /*
     * Client Status Codes: 0 = Start Game
     * 
     * Server Status Codes: 0 = Server Ready, Game initialized 1 = Correct Guess,
     * Client wins! 2 = Incorrect Guess 9 = Game Over
     */
    public static final int START_GAME=0;
    public static final int SERVER_READY=0;
    public static final int CORRECT_GUESS=1;
    public static final int INCORRECT_GUESS=2;
    public static final int GAME_OVER=9;
    
    /* Start app with these parameters:
       - args[0] = hostname, either name or IP address
       - args[1] = port
     */
    public static void main(String[] args) {
        if (args.length<2) {
            System.out.println("Missing parameter: Either hostname or port is missing!");
        } else {
            String serverName = args[0];
            int serverPort = Integer.parseInt(args[1]);
            
            //Init Console
            Scanner console = new Scanner(System.in);
            
            try(Socket gameSocket = new Socket(serverName, serverPort)) {                                
                //init connection streams
                InputStream fromServer = gameSocket.getInputStream();
                OutputStream toServer = gameSocket.getOutputStream();

                //Start game
                toServer.write(START_GAME);
                int serverCode = fromServer.read();
                if (serverCode==SERVER_READY) {
                    System.out.println("Game started!");
                } else {
                    throw new IllegalStateException("Client and Server not synchronized!");
                }
                GAME_LOOP:
                while(true) {
                    System.out.print("Enter your guess (0-9): ");                    
                    toServer.write(console.nextInt());
                    serverCode = fromServer.read();
                    switch(serverCode) {
                        case CORRECT_GUESS:
                            System.out.println("You win! Your guess was right!");
                            break GAME_LOOP;
                        case INCORRECT_GUESS:
                            System.out.println("Wrong Guess! Please, try again!");
                            break;
                        case GAME_OVER:
                            System.out.println("You lost! Too many wrong guesses!");
                            break GAME_LOOP;
                        default:
                            throw new IOException("Invalid Server Code");
                    }
                } 
            } catch (IOException e) {
                System.err.println("Communication problems accured! May be the server is not running or your game is over!");
                e.printStackTrace();
            }
            console.close();
        }
    }
}
