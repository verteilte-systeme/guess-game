# Exercise 1.2
This is a solution to exercise 1.2 (Guess Game basted on TCP) of the hands-on training of lecture **distributed systems**.

Each folder contains a gradle project.
There are two different server implementations:
1. Iterative Server: single threaded solution.
2. Concurrent Server: one dispatcher thread that accepts client requests, and multiple worker threads that play a game with a certain client.

Since both implementations use the same port (7896), only one server can run at a time.

Start the iterative server from a CLI (like bash, cmd, powershell) as follows:
* `$ cd iterative-server`
* `$ gradle run`

That will work, if gradle is installed on your system, otherwise use the gradle wrapper contained in the folder (gradlew.bat, gradlew).

The concurrent server can be started in the same way, just change to folder `concurrent-server` and call `$ gradle run` again.

When the server is up and running, you can start the client from a CLI as often you want. The client needs the endpoint informations:
- `args[0]: hostname of the server`
- `args[1]: server port`

Start the client as follows:
* `$ cd console-client`
* `$ gradle run --args="localhost 7896"`

The client uses the CLI to interact with a user. Enter your guesses right into the console and pay attention to the hints printed in your terminal.
